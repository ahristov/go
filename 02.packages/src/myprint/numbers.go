package myprint

import "fmt"

func PrintSum(a, b, s int) {
	fmt.Printf("%d + %d = %d\n", a, b, s)
}
