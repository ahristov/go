# A Page to Go

## Into

This is a place where I'm experimenting, learning and feeling happy with the Golang.

### About Me

Atanas Hristov

### Repo Set-up

This section contains directions and the shell commands from the initial set-up on my computer and addint the README.md file:


Change to directory where you want to check-out the repository, in my case:

	[ahristov@localhost Projects]$ pwd
	/home/ahristov/Projects

Clone the repository here:

	git clone https://ahristov@bitbucket.org/ahristov/go.git
	cd go

Create new README.md file, add to the repo:

	vim README.md
	...
	git add README.md
	git commit . -m "adding README"

Push to the origin:

	git push -u origin master

Optional: You might want to set-up your git username and email for commits:

    git config --global user.name "Atanas Hristov"
    git config --global user.email atanashristov@hotmail.com

This if the page with instructions from [Atlassian](https://confluence.atlassian.com/display/BITBUCKET/Clone+Your+Git+Repo+and+Add+Source+Files)

You can also set up `.gitignore` file, and add files to ignore:

	$ cat .gitignore 
	*.o
	*.so
	
	$ git add .
	$ git commit . -m "Added Part 02 - Packages"
	$ git push -u origin master
                                                                             



## Go: Intro



### Go: Intro: Set-Up

I prefer downloading and installing latest version of Go instead of using the one from the distro.

Remove install directory if one already exists and copy over the latest distro:

	sudo rm -r /usr/local/go
	sudo tar -C /usr/local -xzf go1.1.linux-amd64.tar.gz

Add `/usr/local/go/bin/` to your $PATH environment variable:

	export GOROOT=/usr/local/go
	export PATH=$PATH:$GOROOT/bin

Check the version of go:

	[ahristov@localhost go]$ . /etc/profile
	[ahristov@localhost go]$ go version

Note: The Go binary distributions assume they will be installed in `/usr/local/go`. 

Follow the [Reference](http://golang.org/doc/install) page for more instructions.


### Go: Intro: Hello Go

The installation of go was easy and painless. Even more pleasure is to write a simple `Hello go` app.

Create hello.go file with the source code:


	[ahristov@localhost 01.intro]$ cat hello.go 
	package main
	
	func main()
	{
	        println("Hello", "Go")
	}


Please note that in Go, the open braces should not be on a new row. Also, Go uses tabs to intend the code and not spaces.

Let's check the format if the code using the Go's gofmt command:


	ahristov@localhost 01.intro]$ gofmt hello.go 
	hello.go:4:1: expected declaration, found '{'

Correct the code:

	package main
	
	func main() {
	        println("Hello", "Go")
	}

It is time to run our first Go program:

	[ahristov@localhost 01.intro]$ go run hello.go 
	Hello Go


Check the source code of the app at: [01.intro/hello.go](01.intro/hello.go)


#### Go: Hello Go: Compilation

We can use the same `go` command with different option `build` to compile the program:

	[ahristov@localhost 01.intro]$ time go build hello.go && time ./hello && ls -lah ./hello
	
	real    0m0.075s
	user    0m0.063s
	sys     0m0.012s
	Hello Go
	
	real    0m0.004s
	user    0m0.003s
	sys     0m0.001s
	-rwxrwxr-x. 1 ahristov ahristov 411K May 26 11:19 ./hello
	
Few things to notice here:

- compilation was fast 
- runtime was short
- binary file relatively big  

How that we compiled the hello.go to binary hello, note the size of the binary - looks pretty big for that small program.

It is that Go will statically link all the necessary runtime to run our app in the binary file.



#### Go: Hello Go: Package fmt

In go we use `packages` to give the code better structure.i Every go file can belong to only and only one go package. 
In the above program we used `package main`. The package to which the file belongs to must be indicated on the very first line.


The "Hello Go" example can be rewritten using the `fmt` package. We import the packages the code in the file needs with `import` keyword:

	[ahristov@localhost 01.intro]$ cat hello_fmt.go 
	package main
	
	import "fmt"
	
	func main() {
	        fmt.Println("Hello, Go")
	}

This is the link to the [documentation](http://godoc.org/fmt) of the fmt package.


Every app has one package `main`, end in that package there must be the main() function. This is the starting point of the app.


Running the [01.intro/hello_fmt.go](01.intro/hello_fmt.go) program results in similar output like before:

	[ahristov@localhost 01.intro]$ go run hello_fmt.go 
	Hello, Go

Of cource the fmt package has a function similar to the printf in C, and this is the `Printf`:

        fmt.Printf("%d + %d = %d\n", a, b, a + b)


### Go: Intro: Packages

#### Go: Packages: 3rd Patry Packages

First step to building 3rd party packages is: set the **GOPATH** environment variable.

To get more information run the `go help` command:

	go help gopath
	
Create directory where to checkout the repositories and set the environment variable from ~/.bash_profile. In my case I'm using ~/goapps

	export GOPATH=$HOME/goapps

The code for package "foo/bar" goes into:

    $HOME/goapps/src/foo/bar/*.go

The compiled binaries go to:

	$HOME/goapps/pkg/GOOS_GOARCH/foo/bar.a

and can be imported as "foo/bar":

	import "foo/bar"



For how to buid a package from github, let's give an example with [randutil](https://github.com/jmcvetta/randutil) - random number/string utilities for the Go language - which documentation can be found [here](http://godoc.org/github.com/jmcvetta/randutil).

To get the `randutil` package from GitHub, run:

	go get github.com/jmcvetta/randutil


If we build it now (optional step, but let's check how it works), it'll produce randutil.a binary as below:

	cd $HOME/goapps/src/github.com/jmcvetta/randutil
	go build
	ls ~/goapps/pkg/linux_amd64/github.com/jmcvetta/
	...
		randutil.a
	
We'll then create new main package and use it from there:

	[ahristov@localhost 02.01.packages.3rdparty]$ cat main.go 
	package main
	
	import (
	        "fmt"
	        "github.com/jmcvetta/randutil"
	)
	
	func main() {
	        var i int
	        i, _ = randutil.IntRange(1, 10)
	        fmt.Printf("1..10: %d\n", i)
	}
	[ahristov@localhost 02.01.packages.3rdparty]$ go run main.go 
	1..10: 5
	

And this is the source code: [02.01.packages.3rdparty/main.go](02.01.packages.3rdparty/main.go)


#### Go: Packages: Application Packages

Having global directory for the packages is fine, but sometimes we may want to include packages below the current directory as we are developing apps. The $GOPATH variable may contain multiple directories divided with column. It may not include relative path like ".". 

There of we could include in the shell account's `.bash_profile` that format of setting the $GOPATH:

[ahristov@localhost 02.packages]$ tail -n 1 ~/.bash_profile 
export GOPATH=$HOME/goapps:`pwd`

Any package that we have should be in a subdirectory under `src` and should have the same name as the package itself. Here is example tree of the project structure:

	[ahristov@localhost 02.packages]$ tree
	.
	├── main
	├── main.go
	└── src
	    ├── mymath
	    │   └── mymath.go
	    └── myprint
	        └── numbers.go
	
In this example we have packages mymath and myprint. One source file can belong to only one package, but the package itself may contain many source files.

Change to the main directory of the application and run:

	[ahristov@localhost 02.packages]$ . ~/.bash_profile; env | grep GO
	GOROOT=/usr/local/go
	GOPATH=/home/ahristov/goapps:/home/ahristov/Projects/go/02.packages

then we can run out example app:

	[ahristov@localhost 02.packages]$ go run main.go 
	10 + 20 = 30

Find the source code [here](02.packages/)



#### Go: Packages: Useful Packages

I found some packages very useful and just want to list them here:

- Markdown processing:

[blackfriday](https://github.com/russross/blackfriday)
[blackfriday-tool](https://github.com/russross/blackfriday-tool)
[md2min](https://github.com/fairlyblank/md2min)

Following is an example how to build the `blackfriday-tool` with:
	
	$ go get github.com/russross/blackfriday-tool
	$ cd ~/goapps/src/github.com/russross/blackfriday-tool
	$ go build -o blackfriday-tool
	$ cp ./blackfriday-tool ~/bin/








