package main

import (
	"fmt"
	"github.com/jmcvetta/randutil"
)

func main() {
	var i int
	i, _ = randutil.IntRange(1, 10)
	fmt.Printf("1..10: %d\n", i)
}
